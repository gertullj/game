use gl;
use sdl2;
use sdl2::event::*;
use sdl2::keyboard::Keycode;
use std::{
    ffi::c_void,
    io::{Read, Result},
};

struct Shader(u32);

impl Shader {
    fn bind(&self) {
        unsafe {
            gl::UseProgram(self.0);
        }
    }
}

const MAX_ROOTS: u32 = 10;

extern "system" fn gl_debug_callback(
    source: u32,
    t: u32,
    id: u32,
    severity: u32,
    length: i32,
    msg: *const i8,
    param: *mut c_void,
) {
    println!(
        "GL: source: {}, type: {}, id: {}, severity: {}:\n\t{}",
        match source {
            gl::DEBUG_SOURCE_API => "API",
            gl::DEBUG_SOURCE_WINDOW_SYSTEM => "WINDOW SYSTEM",
            gl::DEBUG_SOURCE_SHADER_COMPILER => "SHADER COMPILER",
            gl::DEBUG_SOURCE_THIRD_PARTY => "THIRD PARTY",
            gl::DEBUG_SOURCE_APPLICATION => "APPLICATION",
            _ => "UNKNOWN",
        },
        match t {
            gl::DEBUG_TYPE_ERROR => "ERROR",
            gl::DEBUG_TYPE_DEPRECATED_BEHAVIOR => "DEPRECATED BAHAVIOR",
            gl::DEBUG_TYPE_UNDEFINED_BEHAVIOR => "UNDEFINED BEHAVIOR",
            gl::DEBUG_TYPE_PORTABILITY => "PORTABILITY",
            gl::DEBUG_TYPE_PERFORMANCE => "PERFORMANCE",
            gl::DEBUG_TYPE_MARKER => "MARKER",
            _ => "UNKNOWN",
        },
        id,
        match severity {
            gl::DEBUG_SEVERITY_HIGH => "HIGH",
            gl::DEBUG_SEVERITY_MEDIUM => "MEDIUM",
            gl::DEBUG_SEVERITY_LOW => "LOW",
            gl::DEBUG_SEVERITY_NOTIFICATION => "NOTIFICATION",
            _ => "UNKNOWN",
        },
        unsafe {
            std::str::from_utf8_unchecked(std::slice::from_raw_parts(
                msg as *const u8,
                length as usize,
            ))
        }
    )
}

fn load_shader(vertex_path: &str, fragment_path: &str) -> std::io::Result<Shader> {
    let mut vertex_file = std::fs::File::open(vertex_path)?;
    let mut fragment_file = std::fs::File::open(fragment_path)?;

    let mut vertex_src = String::new();
    vertex_file.read_to_string(&mut vertex_src)?;
    vertex_src.push('\0');
    let mut fragment_src = String::new();
    fragment_file.read_to_string(&mut fragment_src)?;
    fragment_src.push('\0');

    let mut prog: u32 = 0;

    unsafe {
        let vs = gl::CreateShader(gl::VERTEX_SHADER);
        let fs = gl::CreateShader(gl::FRAGMENT_SHADER);

        assert_ne!(vs, 0);
        assert_ne!(fs, 0);

        gl::ShaderSource(
            vs,
            1,
            (&(vertex_src.as_ptr() as *const i8) as *const *const i8),
            0 as *const i32,
        );

        gl::ShaderSource(
            fs,
            1,
            (&(fragment_src.as_ptr() as *const i8) as *const *const i8),
            0 as *const i32,
        );

        let mut vs_length: i32 = 0;
        let mut fs_length: i32 = 0;
        gl::GetShaderiv(vs, gl::SHADER_SOURCE_LENGTH, &mut vs_length as *mut i32);
        gl::GetShaderiv(fs, gl::SHADER_SOURCE_LENGTH, &mut fs_length as *mut i32);

        let mut vs_ret: Vec<u8> = Vec::with_capacity(vs_length as usize);
        vs_ret.resize(vs_length as usize, 0);
        let mut fs_ret: Vec<u8> = Vec::with_capacity(fs_length as usize);
        fs_ret.resize(fs_length as usize, 0);

        gl::GetShaderSource(vs, vs_length, 0 as *mut i32, vs_ret.as_mut_ptr() as *mut i8);
        gl::GetShaderSource(fs, fs_length, 0 as *mut i32, fs_ret.as_mut_ptr() as *mut i8);

        assert_eq!(vertex_src.len(), vs_length as usize);
        assert_eq!(fragment_src.len(), fs_length as usize);

        gl::CompileShader(vs);
        gl::CompileShader(fs);

        let mut vs_compile_status: i32 = gl::FALSE as i32;
        let mut fs_compile_status: i32 = gl::FALSE as i32;

        gl::GetShaderiv(vs, gl::COMPILE_STATUS, &mut vs_compile_status as *mut i32);
        gl::GetShaderiv(fs, gl::COMPILE_STATUS, &mut fs_compile_status as *mut i32);

        if vs_compile_status == gl::FALSE as i32 {
            let mut log_length: i32 = 0;
            gl::GetShaderiv(vs, gl::INFO_LOG_LENGTH, &mut log_length as *mut i32);
            let mut log: Vec<u8> = Vec::with_capacity(log_length as usize);
            log.resize(log_length as usize, 0);
            gl::GetShaderInfoLog(vs, log_length, 0 as *mut i32, log.as_mut_ptr() as *mut i8);
            println!(
                "Error compiling vertex shader: {}",
                std::str::from_utf8_unchecked(&*log)
            );
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "Vertex shader could not be compiled.",
            ));
        }

        if fs_compile_status == gl::FALSE as i32 {
            let mut log_length: i32 = 0;
            gl::GetShaderiv(fs, gl::INFO_LOG_LENGTH, &mut log_length as *mut i32);
            let mut log: Vec<u8> = Vec::with_capacity(log_length as usize);
            log.resize(log_length as usize, 0);
            gl::GetShaderInfoLog(fs, log_length, 0 as *mut i32, log.as_mut_ptr() as *mut i8);
            println!(
                "Error compiling fragment shader: {}",
                std::str::from_utf8_unchecked(&*log)
            );
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "Fragment shader could not be compiled.",
            ));
        }
        prog = gl::CreateProgram();
        assert_ne!(prog, 0);

        gl::AttachShader(prog, vs);
        gl::AttachShader(prog, fs);

        gl::LinkProgram(prog);

        let mut link_status: i32 = gl::FALSE as i32;
        gl::GetProgramiv(prog, gl::LINK_STATUS, &mut link_status as *mut i32);

        if link_status == gl::FALSE as i32 {
            let mut log_length: i32 = 0;
            gl::GetProgramiv(prog, gl::INFO_LOG_LENGTH, &mut log_length as *mut i32);
            let mut log: Vec<u8> = Vec::with_capacity(log_length as usize);
            log.resize(log_length as usize, 0);
            gl::GetProgramInfoLog(prog, log_length, 0 as *mut i32, log.as_mut_ptr() as *mut i8);
            println!(
                "Error linking shader program: {}",
                std::str::from_utf8_unchecked(&*log)
            );
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "Program could not be linked.",
            ));
        }

        // int memcpy(char *restrict src, char *restrict dst);

        // int main() {
        //  double a = 0.0;
        //  double b = 1.0;
        //
        //  printf("%f\n", *((&a)+1));
        //
        // }
    }
    Ok(Shader(prog))
}

fn window2math(p: (i32, i32), ws: (u32, u32)) -> (f32, f32) {
    let aspect_ratio: f32 = (ws.0 as f32) / (ws.1 as f32);
    (
        ((p.0 as f32 / (ws.0 as f32) * 2.0) - 1.0) * aspect_ratio,
        ((p.1 as f32 / (ws.1 as f32) * 2.0) - 1.0),
    )
}

fn math2window(p: (f32, f32), ws: (u32, u32)) -> (i32, i32) {
    let aspect_ratio: f32 = (ws.0 as f32) / (ws.1 as f32);
    (
        ((p.0 / aspect_ratio + 1.0) * 0.5 * (ws.0 as f32)) as i32,
        ((p.1 + 1.0) * 0.5 * (ws.1 as f32)) as i32,
    )
}

fn distance(p1: (f32, f32), p2: (f32, f32)) -> f32 {
    ((p1.0 - p2.0) * (p1.0 - p2.0) + (p1.1 - p2.1) * (p1.1 - p2.1)).sqrt()
}

fn main() -> std::io::Result<()> {
    let ctx = sdl2::init().unwrap();
    let video = ctx.video().unwrap();

    let mut win_size: (u32, u32) = (800, 600);

    let window = video
        .window("My Game", win_size.0, win_size.1)
        .resizable()
        .opengl()
        .build()
        .unwrap();

    let gl_attr = video.gl_attr();
    gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
    gl_attr.set_context_version(4, 4);
    gl_attr.set_multisample_samples(4);

    let gl_ctx = window.gl_create_context().unwrap();
    window.gl_make_current(&gl_ctx).unwrap();
    gl::load_with(|s| video.gl_get_proc_address(s) as *const _);

    unsafe {
        gl::Enable(gl::DEBUG_OUTPUT);
        gl::Enable(gl::DEBUG_OUTPUT_SYNCHRONOUS);
        gl::DebugMessageCallback(gl_debug_callback, 0 as *const c_void);

        gl::Viewport(0, 0, win_size.0 as i32, win_size.1 as i32);
        gl::ClearColor(0.0, 0.3, 0.2, 1.0);
    }
    let shader = load_shader("shader.vert", "shader.frag")?;

    const MAGN: f32 = 1.0;

    let quad: Vec<f32> = vec![
        /*ul*/ -MAGN, -MAGN, 0.0, MAGN, MAGN, 0.0, -MAGN, MAGN, 0.0, /*lr*/ MAGN, -MAGN,
        0.0, MAGN, MAGN, 0.0, -MAGN, -MAGN, 0.0,
    ];

    let mut VBO: u32 = 0;
    let mut VAO: u32 = 0;
    VBO = 0;
    VAO = 0;
    unsafe {
        gl::GenVertexArrays(1, &mut VAO as *mut u32);
        gl::GenBuffers(1, &mut VBO as *mut u32);

        assert_ne!(VBO, 0);
        assert_ne!(VAO, 0);

        gl::BindVertexArray(VAO);

        gl::BindBuffer(gl::ARRAY_BUFFER, VBO);

        gl::BufferData(
            gl::ARRAY_BUFFER,
            (quad.len() * std::mem::size_of::<f32>()) as isize,
            quad.as_ptr() as *const c_void,
            gl::STATIC_DRAW,
        );
        gl::EnableVertexAttribArray(0);
        gl::VertexAttribPointer(
            0,
            3,
            gl::FLOAT,
            gl::FALSE,
            (3 * std::mem::size_of::<f32>()) as i32,
            0 as *const c_void,
        );
    }

    let mut ev_pump = ctx.event_pump().unwrap();

    let mut roots: Vec<(f32, f32)> = vec![
        (1.0, 0.0),
        (-0.5, 3.0f32.sqrt() / 2.0),
        (-0.5, -(3.0f32.sqrt() / 2.0)),
    ];

    shader.bind();
    let (roots_location, num_roots_location, win_size_location) = unsafe {
        (
            gl::GetUniformLocation(shader.0, "roots\0".as_ptr() as *const i8),
            gl::GetUniformLocation(shader.0, "num_roots\0".as_ptr() as *const i8),
            gl::GetUniformLocation(shader.0, "win_size\0".as_ptr() as *const i8),
        )
    };

    let mut pressed = false;

    'mainloop: loop {
        for event in ev_pump.poll_iter() {
            match event {
                Event::Quit { .. } => {
                    break 'mainloop;
                }
                Event::Window {
                    win_event: WindowEvent::Resized(x, y),
                    ..
                } => {
                    win_size = (x as u32, y as u32);
                    unsafe {
                        gl::Viewport(0, 0, x, y);
                    }
                }
                Event::KeyDown {
                    keycode: Some(keycode),
                    ..
                } => {
                    if keycode == Keycode::Q {
                        break 'mainloop;
                    }
                }
                Event::MouseButtonDown {
                    mouse_btn: sdl2::mouse::MouseButton::Left,
                    x,
                    y,
                    ..
                } => {
                    pressed = true;
                }
                Event::MouseButtonUp {
                    mouse_btn: sdl2::mouse::MouseButton::Left,
                    x,
                    y,
                    ..
                } => {
                    pressed = false;
                }
                Event::MouseButtonDown {
                    mouse_btn: sdl2::mouse::MouseButton::Right,
                    x,
                    y,
                    ..
                } => {
                    let m = window2math((x, win_size.1 as i32 - y), win_size);
                    let mut some: bool = false;
                    let mut tbd: usize = 0;
                    for (i, root) in roots.iter_mut().enumerate() {
                        if distance(m, *root) < 0.03 {
                            some = true;
                            tbd = i;
                            break;
                        }
                    }
                    if some {
                        roots.remove(tbd);
                    } else if roots.len() < MAX_ROOTS as usize {
                        roots.push(m);
                    }
                }
                Event::MouseMotion {
                    x, y, xrel, yrel, ..
                } => {
                    if pressed {
                        // drag event
                        for (i, root) in roots.iter_mut().enumerate() {
                            let m =
                                window2math((x + xrel, win_size.1 as i32 - (y + yrel)), win_size);
                            if distance(m, *root) < 0.03 {
                                *root = m;
                            }
                        }
                    }
                }
                _ => {}
            }
        }

        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
            // gl::BindVertexArray(VAO);
            // gl::BindBuffer(gl::ARRAY_BUFFER, VBO);
            shader.bind();
            gl::Uniform1i(num_roots_location, roots.len() as i32);
            gl::Uniform2fv(
                roots_location,
                (roots.len() * 2) as i32,
                roots.as_ptr() as *const f32,
            );
            gl::Uniform2i(win_size_location, win_size.0 as i32, win_size.1 as i32);

            // gl::EnableVertexAttribArray(0);
            gl::DrawArrays(gl::TRIANGLES, 0, 12);
        }

        window.gl_swap_window();
    }

    Ok(())
}

#version 440 core
out vec4 FragColor;

uniform vec2 roots[10];
uniform int num_roots;
uniform ivec2 win_size;

float PI = 3.141592;

vec2 comp_add(vec2 a, vec2 b) {
    return vec2(a.x + b.x, a.y + b.y);
}

vec2 comp_sub(vec2 a, vec2 b) {
    return vec2(a.x - b.x, a.y - b.y);
}

vec2 comp_mul(vec2 a, vec2 b) {
    return vec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

vec2 comp_div(vec2 a, vec2 b) {
    float div = b.x * b.x + b.y * b.y;
    return vec2((a.x * b.x + a.y * b.y) / div, (a.y * b.x - a.x * b.y) / div);
}

vec2 comp_exp(vec2 z) {
    float s = exp(z.x);
    return s * vec2(cos(z.y), sin(z.y));
}

vec2 comp_sin(vec2 z) {
    return vec2(sin(z.x) * cosh(z.y), cos(z.x) * sinh(z.y));
}

vec2 comp_cos(vec2 z) {
    return vec2(cos(z.x) * cosh(z.y), sin(z.x) * sinh(z.y));
}

float comp_magn(vec2 z) {
    return length(z);
}

vec2 root1 = vec2(1.0, 0.0);
vec2 root2 = vec2(- 0.5, sqrt(3)*0.5);
vec2 root3 = vec2(- 0.5, - sqrt(3)*0.5);

vec2 function(vec2 z) {
    // (z - r1) (z - r2) (z - r3)
    // return comp_mul(comp_mul(z - root1, z - root2), z - root3);
    vec2 r = vec2(1.0, 0.0);
    for(int i = 0; i < num_roots; i++) {
        r = comp_mul(r, z - roots[i]);
    }
    return r;
    // return comp_exp(z);

}

vec2 dfunction(vec2 z) {
    // (z - r2) (z - r3) + (z - r1)((z - r2) + (z - r3))
    return comp_add(comp_add(comp_mul(z - root2, z - root3), comp_mul(z - root1, z - root2)), comp_mul(z - root1, z - root3));
}

vec2 newton_raphson(vec2 guess, int iters) {
    vec2 x_n = guess;
    for(int i = 0; i < iters; i++){
        float dx = 0.001;
        // vec2 dx_n = dfunction(x_n);
        vec2 dx_n = (function(x_n + vec2(dx * 1.0, dx * 0.0)) - function(x_n)) / dx;
        x_n = x_n - comp_div(function(x_n), dx_n);
    }
    return x_n;
}

vec3 hsv2rgb(vec3 c) {
  vec3 rgb = clamp(abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );
  rgb = rgb*rgb*(3.0-2.0*rgb);
  return c.z * mix( vec3(1.0), rgb, c.y);
}

vec2 window2math(vec2 p) {
    float aspect_ratio = float(win_size.x)/float(win_size.y);
    return vec2(
        ((p.x / float(win_size.x) * 2.0) - 1.0) * aspect_ratio,
        ((p.y / float(win_size.y) * 2.0) - 1.0)
    );
}

vec2 math2window(vec2 p) {
    float aspect_ratio = float(win_size.x)/float(win_size.y);
    return vec2(
        (p.x / aspect_ratio + 1.0) * 0.5 * float(win_size.x),
        (p.y + 1.0) * 0.5 * float(win_size.y));

}

void main() {
    vec2 center = vec2(0.0, 0.0);
    float scale = 1;
    vec2 p = window2math(gl_FragCoord.xy);
    vec2 z = vec2(0.0, 0.0);
    // int i = 0;
    // for(; i < 100; i++) {
    //     z = comp_add(comp_mul(z, z), p);
    //     if(comp_magn(z) > 2.0) break;
    // }
    // vec2 root = p;
    vec2 root = newton_raphson(p, 50);
    float d1 = distance(root, root1);
    float d2 = distance(root, root2);
    float d3 = distance(root, root3);


    float a = atan(root.x, root.y) * PI * 0.05;

    FragColor.xyz = hsv2rgb(vec3(a, 1.0, 0.5));

    for(int i = 0; i < num_roots; i++) {
        if(distance(p, roots[i]) < 0.03) {
            FragColor.xyz += vec3(0.2, 0.2, 0.2);
        }
    }


    // if(d1 < d2 && d1 < d3) {
    //     FragColor = vec4(1.0, 0.3, 0.3, 1.0);
    // } else if(d2 < d1 && d2 < d3) {
    //     FragColor = vec4(0.0, 1.3, 0.3, 1.0);
    // } else {
    //     FragColor = vec4(0.3, 0.3, 1.0, 1.0);
    // }

    // if(abs(int(p.x*100) % 10) < 0.001 || abs(int(p.y*100) % 10) < 0.001) {
    //     FragColor = vec4(0.3, 0.3, 0.3, 1.0);
    // }

    // FragColor = comp_magn(z) > 2.0 ? vec4(0.0, 0.0, 0.0, 1.0) : vec4(1.0, 1.0, 1.0, 1.0);

}

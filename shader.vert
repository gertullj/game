#version 440 core
layout (location = 0) in vec3 coords;
// layout (location = 0) out vec3 pos;

void main() {
    gl_Position = vec4(coords.xyz, 1.0);
    // pos = gl_Position.xyz;
}
